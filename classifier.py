import os
import sklearn
import tensorflow as tf
from tensorflow import keras
from datetime import datetime as dt
import datetime
from tensorflow.keras.layers import Input, Dropout, Dense, LSTM, TimeDistributed, RepeatVector
from tensorflow.keras import layers
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers 

a = datetime.datetime.now()

if not tf.config.list_physical_devices('GPU'):
    print("No GPU was detected. LSTMs and CNNs can be very slow without a GPU.")
gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_visible_devices(gpus[0], 'GPU')
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import rc
from pandas.plotting import register_matplotlib_converters
import pandas as pd

np.random.seed(42)
tf.random.set_seed(42)

import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle
with open('targets.pickle', 'rb') as handle:
    targets = pickle.load(handle)

with open('inputs.pickle', 'rb') as handle:
    inputs = pickle.load(handle)

with open('class_names.pickle', 'rb') as handle:
    class_names = pickle.load(handle)

print(inputs.shape)
targets = np.reshape(targets, (len(targets),1))
print(targets.shape)

from collections import Counter
keys = Counter(targets[:,0]).keys() # equals to list(set(targets[:,0]))
values = Counter(targets[:,0]).values() # counts the elements' frequency
print(keys)
print(values)

idx = np.random.permutation(len(inputs))
cutoff = int(len(idx) * 0.85)
inputs = inputs[idx]
targets = targets[idx]

x_train = inputs[:cutoff]
y_train = targets[:cutoff]

x_validate = inputs[cutoff:]
y_validate = targets[cutoff:]

# def generate_time_series(batch_size, n_steps, freq, freq2):
#     offset1, offset2 = np.random.rand(2, batch_size, 1)
#     time = np.linspace(0, 1, n_steps)
#     series = 0.5 * np.sin((time * - offset1 ) * (freq))
#     series += 0.3 * np.sin((time * - offset2 ) * (freq2))
#     # series += 0.01 * (np.random.rand(batch_size, n_steps) - 0.5)
#     return series[..., np.newaxis].astype(np.float32)

# n_steps = 50
# batch_size = 10240
# freqs = [0.10, 0.30, 0.5, 0.7]
# freqs2 = [7, 5, 3, 1]

# x_train = np.zeros((0, n_steps, 1))
# y_train = np.zeros((0))
# x_validate = np.zeros((0, n_steps, 1))
# y_validate = np.zeros((0))

# for index, freq in enumerate(freqs):
#     x_train = np.vstack([x_train, generate_time_series(batch_size, n_steps, freq, freqs2[index])]) 
#     y_train = np.append(y_train, [index] * batch_size)
#     x_validate = np.vstack([x_validate, generate_time_series(batch_size, n_steps, freq, freqs2[index])]) 
#     y_validate = np.append(y_validate, [index] * batch_size)

# y_train = np.reshape(y_train, (len(y_train),1))
# y_validate = np.reshape(y_validate, (len(y_validate),1))


from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)

enc = enc.fit(y_train)

y_train = enc.transform(y_train)
y_validate = enc.transform(y_validate)

def get_model_conv(x_train, y_train):
    model = keras.Sequential()
    model.add(layers.Conv1D(filters=16, kernel_size=5, activation='relu', input_shape=(x_train.shape[1],x_train.shape[2])))
    model.add(layers.Dropout(0.2))
    model.add(layers.MaxPooling1D(pool_size=2))
    model.add(layers.Conv1D(filters=32, kernel_size=5, activation='relu', kernel_regularizer =tf.keras.regularizers.l2( l=0.01)))
    model.add(layers.Dropout(0.2))
    model.add(layers.MaxPooling1D(pool_size=2))
    model.add(layers.Conv1D(filters=64, kernel_size=5, activation='relu', kernel_regularizer =tf.keras.regularizers.l2( l=0.01)))
    model.add(layers.Dropout(0.2))
    model.add(layers.MaxPooling1D(pool_size=2))
    model.add(layers.Dropout(0.2))
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu', kernel_regularizer =tf.keras.regularizers.l2( l=0.01)))
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(y_train.shape[1], activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])
    return model

def get_model_lstm(x_train, y_train):
    model = keras.Sequential()
    model.add(
        keras.layers.LSTM(
            units=64,
            input_shape=[x_train.shape[1], x_train.shape[2]],
            return_sequences=True
        )
    )
    model.add(
        keras.layers.LSTM(
            units=64
        )
    )
    model.add(keras.layers.Dropout(rate=0.5))
    model.add(keras.layers.Dense(units=128, activation='relu'))
    model.add(keras.layers.Dense(y_train.shape[1], activation='softmax'))

    model.compile(
    loss='categorical_crossentropy',
    optimizer='adam',
    metrics=['acc']
    )
    return model

model = get_model_conv(x_train, y_train)
log_dir = "../logs/fit/" + dt.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath = "../logs/checkpoint",
    save_weights_only = True,
    monitor= "val_loss",
    mode = "min",
    save_best_only=True
)

early_stopping_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0, patience=5, verbose=1,
    mode='min', baseline=None, restore_best_weights=True
)
callbacks = [
    tensorboard_callback,
    model_checkpoint_callback,
    early_stopping_callback
]



history = model.fit(
    x_train, y_train,
    epochs=1000,
    batch_size=32,
    validation_split=0.1,
    shuffle=False,
    callbacks=callbacks
)


    
model.evaluate(x_validate, y_validate)

y_pred = model.predict(x_validate)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig("training.png")
plt.clf()

print(classification_report(enc.inverse_transform(y_validate),enc.inverse_transform(y_pred), target_names=class_names))
print(f"type y_validate {type(y_validate[0][0])}")
print(f"type y_pred {type(y_pred[0][0])}")

def plot_cm(y_true, y_pred, class_names):
    cm = confusion_matrix(y_true, y_pred)
    fig, ax = plt.subplots(figsize=(18, 16)) 
    ax = sns.heatmap(
        cm, 
        annot=True, 
        fmt="d", 
        cmap=sns.diverging_palette(220, 20, n=7),
        ax=ax
    )

    plt.ylabel('Actual')
    plt.xlabel('Predicted')
    ax.set_xticklabels(class_names)
    ax.set_yticklabels(class_names)
    b, t = plt.ylim() # discover the values for bottom and top
    b += 0.5 # Add 0.5 to the bottom
    t -= 0.5 # Subtract 0.5 from the top
    plt.ylim(b, t) # update the ylim(bottom, top) values
    plt.savefig("confusion.png")
    
plot_cm(
    enc.inverse_transform(y_validate),
    enc.inverse_transform(y_pred),
    class_names
)

model_json = model.to_json()
with open("../models/model.json", "w") as js:
    js.write(model_json)

model.save_weights("../models/model.h5")

b = datetime.datetime.now()

c = b-a


print(f"The script took {c.total_seconds()} seconds to run")
