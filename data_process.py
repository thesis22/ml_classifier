from logging import Logger
import os
import pandas as pd



import numpy as np
from pathlib import Path
import random


max_length = 1200
import config.config as config

_logger = config.get_logger()


def find_folders_in_directory(directory):
    items = [os.path.join(directory, item) for item in os.listdir(directory)]
    return [item for item in items if os.path.isdir(item)]

def find_files_with_extension_in_directory(directory, extension):
    items = [os.path.join(directory, item) for item in os.listdir(directory)]
    return [item for item in items if item.endswith(extension)]

def load_data_file(file, max_length, current_oversampling):
    df = pd.read_excel(file, engine = "openpyxl")
    length = len(df.index)
    if length < max_length:
        _logger.info(f"length is only {length}, dropping {file}")
        return None
    df_list = []
    for j in range(current_oversampling):
        current_df = df.copy()
        ran = random.randint(0, length - max_length - 1)
        drop_indices = [i for i in range(length) if i-ran >= max_length or i-ran < 0]
        current_df = current_df.drop(index=drop_indices)
        df_list += [current_df]
    return df_list

def load_all_files_in_folder(folder, max_length, current_oversampling):
    files = find_files_with_extension_in_directory(folder, extension="filled.xlsx")    
    df_list = []
    _logger.info(len(files))
    counter  = 0 
    for file in files:
        _logger.info(f"loading file {file}")
        try:
            data_file = load_data_file(file, max_length, current_oversampling)
            if data_file is not None:
                counter += 1
                df_list += data_file
                _logger.info(f"counter for folder {folder} {counter * current_oversampling}")
        except BaseException as e:
            _logger.info(e)
    _logger.warning(f"Length of data {len(df_list)} from folder {folder}")
    return df_list

def load_all_data(main_folder, max_length, oversampling):
    dataset = {}
    folders = find_folders_in_directory(main_folder)
    oversampling_index = 0
    for folder in folders:
        current_oversampling = oversampling[oversampling_index]
        oversampling_index += 1 
        _logger.info(f"loading folder {folder}")
        dataset[folder] = load_all_files_in_folder(folder, max_length, current_oversampling)
    return dataset

oversampling = [1, 3, 5, 2]
dataset = load_all_data("../data", max_length=max_length, oversampling=oversampling)

def generate_dataset(dataset, max_length, indices):
    inputs = np.zeros((0, max_length, len(indices)))
    outputs = np.zeros((0))
    output_index = 0
    current_index = 0
    for key, val in dataset.items():
        for dataframe in val:
            try:
                current = dataframe[indices].to_numpy()
                current = current[np.newaxis]
                inputs = np.vstack([inputs, current])
                outputs = np.append(outputs, [output_index])
            except:
                _logger.warning(f"Unable to parse data from {key}")
        output_index += 1
    return inputs, outputs

def get_length(x):
    return np.linalg.norm(x)

def get_length_of_combined_data(dataset, index, indices):
    for data in dataset:
        try:
            data[index] = data.apply(lambda x: get_length(data[indices]), axis=1)
        except BaseException as e:
            _logger.info(e)
    return dataset

dataset_lengths_indices = [
    "LSM6DSL Acceleration Sensor",
    "LSM6DSL Gyroscope Sensor",
    "Orientation Sensor",
]



def apply_length_of_sensors(dataset, dataset_lengths_indices, length):
    for key, data in dataset.items():
        for index in dataset_lengths_indices:
            indices = [index + "_" + str(x) for x in range(length)]
            data = get_length_of_combined_data(data, index, indices)
        dataset[key] = data
    return dataset

# dataset = apply_length_of_sensors(dataset, dataset_lengths_indices, 3)

dataset_lengths_indices_prev =[
    "LSM6DSL Acceleration Sensor_0",
    "LSM6DSL Acceleration Sensor_1",
    "LSM6DSL Acceleration Sensor_2",
    "LSM6DSL Gyroscope Sensor_0",
    "LSM6DSL Gyroscope Sensor_1",
    "LSM6DSL Gyroscope Sensor_2",
    "Gravity Sensor_0",
    "Gravity Sensor_1",
    "Gravity Sensor_2",
]




_logger.warning(f"Sice of dataset {len(dataset)}")

inputs, targets = generate_dataset(dataset, max_length, dataset_lengths_indices_prev)

inputs_out = np.zeros((inputs.shape[0], inputs.shape[1], 2))

inputs_out[:,:,0] = np.linalg.norm(inputs[:,:,0:3] - inputs[:,:,6:9], axis=2)
inputs_out[:,:,1] = np.linalg.norm(inputs[:,:,3:6], axis=2)

_logger.info(f"shape {inputs_out.shape}")

_logger.info(f"shape {targets.shape}")


import pickle
with open('inputs_test_norm.pickle', 'wb') as handle:
    pickle.dump(inputs_out, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open('targets.pickle', 'wb') as handle:
    pickle.dump(targets, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open("class_names.pickle", "wb") as handle:
    pickle.dump(list(dataset.keys()), handle, protocol=pickle.HIGHEST_PROTOCOL)